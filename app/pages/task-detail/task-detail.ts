import {NavController, NavParams, Page} from 'ionic-framework/ionic';
//import {SessionDetailPage} from '../session-detail/session-detail';


@Page({
  templateUrl: 'build/pages/task-detail/task-detail.html'
})
export class TaskDetailPage {
  task: any;

  constructor(private nav: NavController, private navParams: NavParams) {
    this.task = this.navParams.data;
  }

  /*
  goToSessionDetail(session) {
    this.nav.push(SessionDetailPage, session);
  }
  */
}
