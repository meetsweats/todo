import {NavController, Page, ActionSheet, Modal} from 'ionic-framework/ionic';
import {TaskData} from '../../providers/task-data';
import {TaskDetailPage} from '../task-detail/task-detail';
import {Task} from "../../interfaces/interfaces";

import {TimeAgoPipe, CalendarPipe} from 'angular2-moment';
import Moment = require("moment/moment");

import {TaskAddPage} from "../task-add/task-add";

import {ArraySortPipe} from "./array-sort";

@Page({
  templateUrl: 'build/pages/task-list/task-list.html',
  pipes: [TimeAgoPipe, CalendarPipe, ArraySortPipe],
})
export class TaskListPage {
  actionSheet: ActionSheet;
  tasks = [];
  title: string;

  taskData: TaskData;
  moment: Moment;

  constructor(private nav: NavController, taskData: TaskData) {
    //TODO: Update provider
    /*
    confData.getSpeakers().then(tasks => {
      this.tasks = tasks;
    });
    */
    this.title = '';
    this.taskData = taskData;

    this.moment = Moment; // we're using moment to show fancy time


    taskData.tasks$.subscribe(latestCollection => {
      this.tasks = latestCollection;
    });
    taskData.loadTasks();

  }


  presentAdd() {
    let modal = Modal.create(TaskAddPage, this.taskData);
    this.nav.present(modal);

    modal.onDismiss((data: any[]) => {
      if (data) {
        //this.excludeTracks = data;
        //this.updateSchedule();
        console.log(data);
      }
    });

  }

  quickAdd(keyCode){
    if(keyCode == 13){
      this.createTask();
      this.title = null;
    }
  }

  checkEnter(keyCode, task){
    if(keyCode == 13){
      this.updateTask(task);
    }
  }

  createTask() {
    let task: Task;
    task = {
      title: this.title,
      createdBy: 5
    };
    console.log('creating', task);
    this.taskData.createTask(task);
  }

  updateTask(task) {
    console.log('updating', task);
    this.taskData.updateTask(task);
  }

  deleteTask(task) {
    this.taskData.deleteTask(task);
  }

  goToTaskDetail(taskName: string) {
    this.nav.push(TaskDetailPage, taskName);
  }

  static goToTaskTwitter(task) {
    window.open(`https://twitter.com/${task.twitter}`);
  }

}
