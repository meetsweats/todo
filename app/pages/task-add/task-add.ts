import {Page, NavParams, ViewController} from 'ionic-framework/ionic';
import {TaskData} from '../../providers/task-data';
import {Task} from "../../interfaces/interfaces";

@Page({
  templateUrl: 'build/pages/task-add/task-add.html',
})
export class TaskAddPage {

  task = {};
  tasks = [];

  constructor(
    private taskData: TaskData,
    private navParams: NavParams,
    private viewCtrl: ViewController
  ){


    //TODO: Update provider
    /*
    confData.getSpeakers().then(tasks => {
      this.tasks = tasks;
    });
    */
    this.task = {
      title: 'My Task',
      due: false,
      createdBy: 0,
    };

    //this.taskData = taskData;

    taskData.tasks$.subscribe(latestCollection => {
      this.tasks = latestCollection;
    });
    taskData.loadTasks();

  }

  dismiss(data) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss(data);
  }

  createTask() {
    //let task: Task;
    let task = {
      title: this.task.title,
      createdBy: this.task.createdBy
    };
    console.log('creating', task);
    this.taskData.createTask(task);
    this.dismiss(task);
  }


}
