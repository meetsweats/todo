export interface Task {
    key?: number,
    title: string,
    created?: Date,
    updated?: Date,
    due?: Date,
    createdBy: number,
}

/*
 completed: Boolean;
export enum Task {
    SOMETHING
};
*/