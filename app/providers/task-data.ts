// Reference: https://coryrylan.com/blog/angular-2-observable-data-services

import {Http, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
//import 'rxjs/Rx' // for all
import {Task} from '../interfaces/interfaces';
import {Injectable} from 'angular2/core';

//import {Moment} from 'moment/moment'; //TODO: Moment

@Injectable()
export class TaskData {
    tasks$: Observable<Array<Task>>;
    private _tasksObserver: any;
    private _dataStore: {
        tasks: Array<Task>
    };

    private API_KEY: string;
    private API_URL: string;

    //moment : Moment; //TODO: Moment
    _headers : any;

    constructor(private _http: Http) {

        const API_KEY = 'b1ac1cfe72bc9da11e70715a07ce48f61820d5a6aceb75eb69cff79589d8059e';
        this.API_URL = 'http://baas-meetsweats.rhcloud.com/api/v2';

        this._headers = new Headers();
        this._headers.append('X-DreamFactory-Api-Key', API_KEY);

        this.tasks$ = new Observable(observer =>
            this._tasksObserver = observer).share();

        this._dataStore = { tasks: [] };
    }

    loadTasks() {
        this._http.get(`${this.API_URL}/db/_table/todo`,{
            headers: this._headers
        }).map(response => response.json().resource).subscribe(data => {
            console.log('loading data', data);

            // TODO: Properly use moment to convert date
            /*
            //transform the date to local using Moment
            data.forEach((t, index) => {
                let updated = moment.utc(t.updated).toDate();
                t.updated = moment(updated).format();
            });
            */

            this._dataStore.tasks = data; // DreamFactory returns a resource object
            this._tasksObserver.next(this._dataStore.tasks);
        }, error => console.log('Could not load tasks.'));
    }

    createTask(task: Task) {
        let resource = [task];
        console.log('resource', resource);
        this._http.post(`${this.API_URL}/db/_table/todo?fields=*`, JSON.stringify(resource), {
                headers: this._headers
            } )
            .map(response => response.json().resource.pop()).subscribe(data => {  // we return the first resource using pop
            console.log('created', data);
            this._dataStore.tasks.push(data);
            this._tasksObserver.next(this._dataStore.tasks);
        }, error => console.log('Could not create task.'));
    }

    updateTask(task: Task) {
        // ?fields=* returns all of the fields of the record. We use this to update our local instance of the task otherwise they get out of sync
        this._http.put(`${this.API_URL}/db/_table/todo/${task.key}?fields=*`, JSON.stringify(task), {
                headers: this._headers
            })
            .map(response => response.json()).subscribe(data => {
            console.log('updated', data);
            this._dataStore.tasks.forEach((task, i) => {
                if (task.key === data.key) { this._dataStore.tasks[i] = data; }
            });

            this._tasksObserver.next(this._dataStore.tasks);
        }, error => console.log('Could not update task.', error));
    }

    deleteTask(task: Task) {
        console.log(task);
        this._http.delete(`${this.API_URL}/db/_table/todo/${task.key}?fields=*`, {
            headers: this._headers
        }).subscribe(response => {
            console.log('deleted', response);
            this._dataStore.tasks.forEach((t, index) => {
                if (t.key === task.key) { this._dataStore.tasks.splice(index, 1); }
            });

            this._tasksObserver.next(this._dataStore.tasks);
        }, error => console.log('Could not delete task.'));
    }
}